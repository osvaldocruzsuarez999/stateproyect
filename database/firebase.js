import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyAw5omY202smMERxmfDLD3JizM7wut5lPo",
    authDomain: "stateproyect.firebaseapp.com",
    projectId: "stateproyect",
    storageBucket: "stateproyect.appspot.com",
    messagingSenderId: "547726105853",
    appId: "1:547726105853:web:f5f6941dfc352e11027294",
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const auth = firebase.auth();
const storage = firebase.storage();
export default {
    firebase,
    db,
    auth,
    storage,
};