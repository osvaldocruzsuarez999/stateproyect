import React, { useEffect,useState } from "react";
import {
  Button,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  Text
} from "react-native";

import firebase from "../database/firebase";

const AddUserScreen = (props) => {
  
  const [stateNombre, setStateNombre] = useState("");
  const [stateCorreo, setStateCorreo] = useState("");
  const [stateTelefono, setStateTelefono] = useState("");

  const initalState = {
    name: "",
    email: "",
    phone: "",
  };
  const [state, setState] = useState(initalState);
  const [viewState,setViewState]=useState(false)
  const handleChangeText = (value, name) => {
    setState({ ...state, [name]: value });
  };
  
useEffect(()=>{
printusers()  
})
  const printusers = async ()=>{
    if (state.name === "") {
      
    } else {
      setViewState(true)
      setStateNombre(state.name)
      setStateTelefono(state.phone)
      setStateCorreo(state.email)
    }

  }
  const saveNewUser = async () => {
    if (state.name === "") {
      alert("please provide a name");
    } else {

      try {
        await firebase.db.collection("users").add({
          name: state.name,
          email: state.email,
          phone: state.phone,
        });

        props.navigation.navigate("UsersList");
      } catch (error) {
        console.log(error)
      }
    }
  };

  return (
    <ScrollView style={styles.container}>
      {/* Name Input */}
      <View style={styles.inputGroup}>
        <TextInput
          placeholder="Nombre"
          onChangeText={(value) => handleChangeText(value, "name")}
          value={state.name}
        />
      </View>

      {/* Email Input */}
      <View style={styles.inputGroup}>
        <TextInput
          placeholder="Correo"
          multiline={true}
          numberOfLines={4}
          onChangeText={(value) => handleChangeText(value, "email")}
          value={state.email}
        />
      </View>

      {/* Input */}
      <View style={styles.inputGroup}>
        <TextInput
          placeholder="Telefono"
          onChangeText={(value) => handleChangeText(value, "phone")}
          value={state.phone}
        />
      </View>
      <View style={styles.button}>
        <Button title="Guardar usuario" onPress={() => saveNewUser()} />
      </View>
      
      
      {viewState==true?(
        <View style={{flex:1, backgroundColor:"blue", marginTop:50}}>
<Text style={{fontSize:30 ,color:"#fff"}}>El estado se activo {stateNombre} {stateTelefono} {stateCorreo} </Text>

</View>
      ):(
        
        <View style={{flex:1, backgroundColor:"red", marginTop:50}}>
<Text style={{fontSize:30 }}>El estado esta en falso hasta que llenes datos</Text>
      
        </View>
      
)}
    

        
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
  },
  inputGroup: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc",
  },
  loader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default AddUserScreen;
